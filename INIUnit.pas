unit INIUnit;

interface

uses System.Classes;

type
  TINIFile = class
    Sections: TStrings;
    procedure LoadFromFile(sFileName: string);

    procedure ReadSections(ss: TStrings);
    procedure ReadSection(SectionName: string; ss: TStrings);

    constructor Create(sFileName: string);
    destructor Destroy; override;
  end;

implementation

{ TINIFile }

constructor TINIFile.Create;
begin
  // Sections := TDictionary<String, TStrings>.Create;
  Sections := TStringList.Create;
  LoadFromFile(sFileName);
end;

destructor TINIFile.Destroy;
begin
  Sections.Free;

  inherited;
end;

procedure TINIFile.LoadFromFile(sFileName: string);
var
  ss, ts: TStrings;
  i: integer;
  s, sSectionName: string;
begin
  if length(sFileName) = 0 then
    exit;

  ss := TStringList.Create;
  ss.LoadFromFile(sFileName);

  for i := 0 to ss.Count - 1 do
  begin
    s := ss[i];
    if pos('[', s) > 0 then
    begin
      ts := TStringList.Create;
      // Sections.Add(sSectionName, ts);
      sSectionName := copy(s, 2, length(s) - 2);
      Sections.AddObject(sSectionName, ts);
    end
    else
    begin
      if Assigned(ts) then
        ts.Add(s);
    end;
  end;
  // Sections.Add(sSectionName, ts);
  // Sections.AddObject(sSectionName, ts);
end;

procedure TINIFile.ReadSection(SectionName: string; ss: TStrings);
var
  idx: integer;
begin
  // ss.Clear;
  // ss.AddStrings(Sections.Items[SectionName]);
  idx := Sections.IndexOf(SectionName);

  if (idx >= 0) then
    ss.AddStrings(Sections.Objects[idx] as TStrings);
end;

procedure TINIFile.ReadSections(ss: TStrings);
var
  i: integer;
begin
  ss.Clear;
  ss.AddStrings(Sections);
  // for i := 0 to Sections.Count - 1 do
  // begin
  // ss.Add(Sections.Keys.ToArray[i]);
  // end;
end;

end.
