program hostSwitcher;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {MainForm},
  INIUnit in 'INIUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
