unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, INIUnit;

type
  TMainForm = class(TForm)
    Memo1: TMemo;
    ListBox1: TListBox;
    btnApply: TButton;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Memo2: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    sINI: TINIFile;
    procedure LoadHostFile;
    procedure SaveHostFile;

  end;

var
  MainForm: TMainForm;

const
  sHostFilePath = 'C:\Windows\System32\drivers\etc\hosts.';
  sHostInIFile = 'Hosts.INI';

implementation

{$R *.dfm}

procedure TMainForm.btnApplyClick(Sender: TObject);
begin
  SaveHostFile;
  LoadHostFile;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  FN: string;
begin
  FN := ExtractFilePath(Application.ExeName) + sHostInIFile;

  if FileExists(FN) then
  begin
    sINI := TINIFile.Create(FN);
    sINI.ReadSections(ListBox1.Items);
  end;

  Font.Name := 'Tahoma';
  Font.Size := 10;

  LoadHostFile;
end;

procedure TMainForm.ListBox1Click(Sender: TObject);
var
  sSecName: string;
begin
  Memo1.Lines.Clear;

  sSecName := ListBox1.Items[ListBox1.ItemIndex];

  if CompareText(sSecName, 'default') <> 0 then
    sINI.ReadSection('default', Memo1.Lines);

  sINI.ReadSection(sSecName, Memo1.Lines);
end;

procedure TMainForm.LoadHostFile;
begin
  Memo2.Lines.LoadFromFile(sHostFilePath);
end;

procedure TMainForm.SaveHostFile;
begin
  Memo1.Lines.SaveToFile(sHostFilePath);
end;

end.
