object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = 'HostSwitcher'
  ClientHeight = 480
  ClientWidth = 543
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 177
    Height = 480
    Align = alLeft
    Caption = ' Host List '
    TabOrder = 0
    ExplicitHeight = 382
    object ListBox1: TListBox
      Left = 2
      Top = 15
      Width = 173
      Height = 463
      Align = alClient
      ItemHeight = 13
      TabOrder = 0
      OnClick = ListBox1Click
      ExplicitHeight = 365
    end
  end
  object Panel1: TPanel
    Left = 177
    Top = 0
    Width = 366
    Height = 480
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 1
    ExplicitHeight = 382
    object GroupBox2: TGroupBox
      Left = 1
      Top = 312
      Width = 364
      Height = 167
      Align = alBottom
      Caption = 'Host File'
      TabOrder = 0
      object Memo2: TMemo
        Left = 2
        Top = 15
        Width = 360
        Height = 150
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
        ExplicitHeight = 246
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 1
      Width = 364
      Height = 311
      Align = alClient
      Caption = 'Generated Host File'
      TabOrder = 1
      ExplicitTop = 49
      ExplicitHeight = 325
      object Memo1: TMemo
        Left = 2
        Top = 15
        Width = 360
        Height = 294
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
        ExplicitLeft = 1
        ExplicitTop = 11
      end
      object btnApply: TButton
        Left = 256
        Top = 28
        Width = 75
        Height = 25
        Caption = 'Apply'
        TabOrder = 1
        OnClick = btnApplyClick
      end
    end
  end
end
